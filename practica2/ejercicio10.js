let longitud = 0;
let area = 0;
let volumen = 0;
let radio = 0;

radio = prompt("Introduce el radio");

area = Math.PI * radio ** 2;
longitud = 2 * Math.PI * radio;
volumen = 4 / 3 * Math.PI * radio ** 3

document.write("Area: " + area + "<br>");
document.write("Longitud: " + longitud + "<br>");
document.write("Volumen: " + volumen + "<br>");